#include "parabolic2d.h"

void Solver::TMAHorizontal(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, Layer &layer, int col, int size)
{
    VecDouble alpha(size + 1);
    VecDouble beta(size + 1);

    alpha[1] = -upper[0] / diag[0];
    beta[1] = rhs[0] / diag[0];

    for (int i = 1; i < size; ++i)
    {
        double denominator = lower[i] * alpha[i] + diag[i];
        alpha[i + 1] = -upper[i] / denominator;
        beta[i + 1] = (rhs[i] - lower[i] * beta[i]) / denominator;
    }

    layer[size - 1][col] = beta[size];
    for (int i = size - 2; i >= 0; --i)
    {
        layer[i][col] = alpha[i + 1] * layer[i + 1][col] + beta[i + 1];
    }
}

void Solver::TMAVertical(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, Layer &layer, int row, int size) {
    VecDouble alpha(size + 1);
    VecDouble beta(size + 1);

    alpha[1] = -upper[0] / diag[0];
    beta[1] = rhs[0] / diag[0];

    for (int i = 1; i < size; ++i) {
        double denominator = lower[i] * alpha[i] + diag[i];
        alpha[i + 1] = -upper[i] / denominator;
        beta[i + 1] = (rhs[i] - lower[i] * beta[i]) / denominator;
    }

    layer[row][size - 1] = beta[size];
    for (int i = size - 2; i >= 0; --i)
    {
        layer[row][i] = alpha[i + 1] * layer[row][i + 1] + beta[i + 1];
    }
}

void Solver::ADIMethod(Parabolic2DEquation &eq, int x_steps, int y_steps, int t_steps, double t_max,
                       std::function<double (double, double, double)> &solution, SolutionRecord &solution_record)
{
    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;
    double hx2 = hx * hx;
    double hy2 = hy * hy;
    double tau = t_max / t_steps;
    Layer curlayer(x_steps + 1, VecDouble(y_steps + 1));
    Layer sublayer(x_steps + 1, VecDouble(y_steps + 1));
    solution_record.hx = hx;
    solution_record.hy = hy;
    solution_record.tau = tau;
    solution_record.x_steps = x_steps;
    solution_record.y_steps = y_steps;
    solution_record.t_steps = t_steps;
    solution_record.errors = VecDouble(t_steps + 1);
    for (int i = 0; i <= x_steps; ++i) {
        for(int j = 0; j <= y_steps; ++j) {
            curlayer[i][j] = eq.phi(hx * i, hy * j);
        }
    }
    solution_record.layer1 = curlayer;
    int vector_size = std::max(x_steps, y_steps) + 1;
    VecDouble lower(vector_size);
    VecDouble upper(vector_size);
    VecDouble diag(vector_size);
    VecDouble rhs(vector_size);
    double max_error = 0;
    for (int k = 0; k < t_steps; ++k) {
        for (int i = 1; i < x_steps; ++i) {
            lower[i] = eq.a / hx2;
            upper[i] = eq.a / hx2;
            diag[i] = -2*eq.a/hx2 - 2/tau;
        }
        for (int j = 1; j < y_steps; ++j) {
            for (int i = 1; i < x_steps; ++i) {
                rhs[i] = - eq.f(hx*i, hy*j, tau*k + tau/2);
                rhs[i] -= eq.a * curlayer[i][j - 1] / hy2;
                rhs[i] -= (-2*eq.a/hy2 + 2/tau) * curlayer[i][j];
                rhs[i] -= eq.a * curlayer[i][j + 1] / hy2;
            }
            diag[0] = eq.beta1 * hx - eq.alpha1;
            upper[0] = eq.alpha1;
            rhs[0] = hx * eq.gamma1(hy*j, tau*k + tau/2);
            diag[x_steps] = eq.beta2 * hx + eq.alpha2;
            lower[x_steps] = -eq.alpha2;
            rhs[x_steps] = hx*eq.gamma2(hy*j, tau*k + tau/2);
            TMAHorizontal(lower, diag, upper, rhs, sublayer, j, x_steps + 1);
        }
        for (int i = 0; i <= x_steps; ++i) {
             sublayer[i][0] = (-eq.alpha3*sublayer[i][1] + hy*eq.gamma3(hx*i, tau*k + tau/2))/(eq.beta3 * hy - eq.alpha3);
             sublayer[i][y_steps] = (eq.alpha4*sublayer[i][y_steps - 1] + hy*eq.gamma4(hx*i, tau*k + tau/2))/(eq.beta4 * hy + eq.alpha4);
        }
        for (int j = 1; j < x_steps; ++j) {
            lower[j] = eq.a / hy2;
            upper[j] = eq.a / hy2;
            diag[j] = -2*eq.a/hy2 - 2/tau;
        }
        for (int i = 1; i < x_steps; ++i) {
            for (int j = 1; j < y_steps; ++j) {
                 rhs[j] = -eq.f(hx * i, hy * j, k * tau + tau/2);
                 rhs[j] -= eq.a*sublayer[i - 1][j]/hx2;
                 rhs[j] -= (-2*eq.a/hx2 + 2/tau)*sublayer[i][j];
                 rhs[j] -= eq.a*sublayer[i + 1][j]/hx2;
             }
             diag[0] = eq.beta3 * hy - eq.alpha3;
             upper[0] = eq.alpha3;
             rhs[0] = hy * eq.gamma3(hx * i, tau*k + tau);
             diag[y_steps] = eq.beta4 * hy + eq.alpha4;
             lower[y_steps] = -eq.alpha4;
             rhs[y_steps] = hy * eq.gamma4(hx * i, tau*k + tau);
             TMAVertical(lower, diag, upper, rhs, curlayer, i, y_steps + 1);
        }
        for (int j = 0; j <= y_steps; ++j) {
            curlayer[0][j] = (-eq.alpha1*curlayer[1][j] + hx*eq.gamma1(hy*j, tau*k + tau)) / (eq.beta1*hx - eq.alpha1);
            curlayer[x_steps][j] = (eq.alpha2*curlayer[x_steps - 1][j] + hx*eq.gamma2(hy*j, tau*k + tau)) / (eq.beta2*hx + eq.alpha2);
        }
        double max_layer_err = 0.0;
        for (int i = 0; i <= x_steps; ++i) {
            for (int j = 0; j <= y_steps; ++j) {
                double uij = solution(i*hx, j*hy, tau*k + tau);
                double err = std::abs(curlayer[i][j] - uij);
                max_layer_err = std::max(max_layer_err, err);
            }
        }
        max_error = std::max(max_layer_err, max_error);
        if ( k == t_steps / 2 )
        {
            solution_record.layer2 = curlayer;
        }
        if (k == t_steps - 1 )
        {
            solution_record.layer3 = curlayer;
        }
        solution_record.errors[k + 1] = max_layer_err;
    }
    solution_record.max_error = max_error;
    std::cerr << "[ADI] Evaluation complete. Max abs error is " << max_error << std::endl;
}

void Solver::FSMethod(Parabolic2DEquation &eq, int x_steps, int y_steps, int t_steps, double t_max,
                      std::function<double (double, double, double)> &solution, SolutionRecord &solution_record)
{
    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;
    double hx2 = hx * hx;
    double hy2 = hy * hy;
    double tau = t_max / t_steps;
    Layer curlayer(x_steps + 1, VecDouble(y_steps + 1));
    solution_record.hx = hx;
    solution_record.hy = hy;
    solution_record.tau = tau;
    solution_record.x_steps = x_steps;
    solution_record.y_steps = y_steps;
    solution_record.t_steps = t_steps;
    solution_record.errors = VecDouble(t_steps + 1);
    for (int i = 0; i <= x_steps; ++i) {
        for(int j = 0; j <= y_steps; ++j) {
            curlayer[i][j] = eq.phi(hx * i, hy * j);
        }
    }
    solution_record.layer1 = curlayer;
    int vector_size = std::max(x_steps, y_steps) + 1;
    VecDouble lower(vector_size);
    VecDouble upper(vector_size);
    VecDouble diag(vector_size);
    VecDouble rhs(vector_size);
    double max_error = 0;
    for (int k = 0; k < t_steps; ++k)
    {
        for (int i = 1; i < x_steps; ++i) {
            lower[i] = eq.a / hx2;
            upper[i] = eq.a / hx2;
            diag[i] = -2*eq.a/hx2 - 1/tau;
        }
        for (int j = 1; j < y_steps; ++j) {
            for (int i = 1; i < x_steps; ++i) {
                rhs[i] = - eq.f(hx*i, hy*j, tau*k)/2 - curlayer[i][j]/tau;
            }
            diag[0] = eq.beta1 * hx - eq.alpha1;
            upper[0] = eq.alpha1;
            rhs[0] = hx * eq.gamma1(hy*j, tau*k + tau);
            diag[x_steps] = eq.beta2 * hx + eq.alpha2;
            lower[x_steps] = -eq.alpha2;
            rhs[x_steps] = hx*eq.gamma2(hy*j, tau*k + tau);
            TMAHorizontal(lower, diag, upper, rhs, curlayer, j, x_steps + 1);
        }
        for (int i = 0; i <= x_steps; ++i) {
             curlayer[i][0] = (-eq.alpha3*curlayer[i][1] + hy*eq.gamma3(hx*i, tau*k + tau/2))/(eq.beta3 * hy - eq.alpha3);
             curlayer[i][y_steps] = (eq.alpha4*curlayer[i][y_steps - 1] + hy*eq.gamma4(hx*i, tau*k + tau/2))/(eq.beta4 * hy + eq.alpha4);
        }
        for (int j = 1; j < x_steps; ++j) {
            lower[j] = eq.a / hy2;
            upper[j] = eq.a / hy2;
            diag[j] = -2*eq.a/hy2 - 1/tau;
        }
        for (int i = 1; i < x_steps; ++i) {
            for (int j = 1; j < y_steps; ++j) {
                 rhs[j] = -eq.f(hx * i, hy * j, k * tau + tau) / 2 - curlayer[i][j]/tau;
             }
             diag[0] = eq.beta3 * hy - eq.alpha3;
             upper[0] = eq.alpha3;
             rhs[0] = hy * eq.gamma3(hx * i, tau*k + tau);
             diag[y_steps] = eq.beta4 * hy + eq.alpha4;
             lower[y_steps] = -eq.alpha4;
             rhs[y_steps] = hy * eq.gamma4(hx * i, tau*k + tau);
             TMAVertical(lower, diag, upper, rhs, curlayer, i, y_steps + 1);
        }
        for (int j = 0; j <= y_steps; ++j) {
            curlayer[0][j] = (-eq.alpha1*curlayer[1][j] + hx*eq.gamma1(hy*j, tau*k + tau)) / (eq.beta1*hx - eq.alpha1);
            curlayer[x_steps][j] = (eq.alpha2*curlayer[x_steps - 1][j] + hx*eq.gamma2(hy*j, tau*k + tau)) / (eq.beta2*hx + eq.alpha2);
        }
        double max_layer_err = 0.0;
        for (int i = 0; i <= x_steps; ++i) {
            for (int j = 0; j <= y_steps; ++j) {
                double uij = solution(i*hx, j*hy, tau*k + tau);
                double err = std::abs(curlayer[i][j] - uij);
                max_layer_err = std::max(max_layer_err, err);
            }
        }
        max_error = std::max(max_layer_err, max_error);
        solution_record.errors[k + 1] = max_layer_err;
    }
    solution_record.max_error = max_error;
    std::cerr << "[FS] Evaluation complete. Max abs error is " << max_error << std::endl;
}

void TestMethods(int x_steps, int y_steps, int t_steps)
{
    Parabolic2DEquation eq(
            1.0, 0.0, 0.0, 0.0, [](double x, double y, double t){ return - x * y * sin(t);},
            [](double x, double y){ return x * y;},
            0.0, 1.0, [](double y, double t){ return 0 /* y*cos(t) */; },
            0.0, 1.0, [](double y, double t){ return 1.0 * y*cos(t); },
            0.0, 1.0, [](double x, double t){ return 0 /* x*cos(t) */;},
            0.0, 1.0, [](double x, double t){ return 1.0 * x*cos(t); },
            1.0, 1.0
    );
//    Parabolic2DEquation eq(
//                1.0, 0.0, 0.0, 0.0, [](double x, double y, double t){return 0;},
//                [](double x, double y){return cos(2*x) * cosh(y);},
//                0.0, 1.0, [](double y, double t){return cosh(y) * exp(-3*t);},
//                0.0, 1.0, [](double y, double t){return 0;},
//                0.0, 1.0, [](double x, double t){return cos(2*x) * exp(-3*t);},
//                0.0, 1.0, [](double x, double t){return (5.0/4.0) * cos(2*x) * exp(-3*t);},
//                M_PI_4, log(2)
//    );
    std::function<double(double, double, double)> solution([](double x, double y, double t){return x * y * cos(t);});
//    std::function<double(double, double, double)> solution([](double x, double y, double t){return cosh(y) * cos(2*x) * exp(-3*t);});
    double t_max = 0.2;
    Solver solver;
    SolutionRecord solution_record;
    solver.ADIMethod(eq, x_steps, y_steps, t_steps, t_max, solution, solution_record);
    solver.FSMethod(eq, x_steps, y_steps, t_steps, t_max, solution, solution_record);
}
