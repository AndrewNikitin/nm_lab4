#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "parabolic2d.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(Parabolic2DEquation eq, std::function<double(double, double, double)> solution, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_calc_button_clicked();

    void on_prev_button_clicked();

    void on_next_button_clicked();

private:
    Parabolic2DEquation eq;
    std::function<double(double, double, double)> solution;
    SolutionRecord last_solution_adi;
    SolutionRecord last_solution_fs;

    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
