#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(Parabolic2DEquation eq, std::function<double(double, double, double)> solution, QWidget *parent)
    : QMainWindow(parent)
    , eq(eq)
    , solution(solution)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->error_plot->addGraph();
    ui->error_plot->addGraph();
    ui->error_plot->graph(0)->setPen(QPen(Qt::blue));
    ui->error_plot->graph(1)->setPen(QPen(Qt::red));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_calc_button_clicked()
{
    Solver solver;
    int x_steps = ui->nx->value();
    int y_steps = ui->ny->value();
    int t_steps = ui->nt->value();
    solver.ADIMethod(eq, x_steps, y_steps, t_steps, 1.0, solution, last_solution_adi);
    solver.FSMethod(eq, x_steps, y_steps, t_steps, 1.0, solution, last_solution_fs);

    QVector<double> t(t_steps + 1);
    for (int i = 0; i <= t_steps; ++i) {
        t[i] = i * last_solution_adi.tau;
    }
    last_solution_fs.errors[0] = 0.0;
    last_solution_adi.errors[0] = 0.0;
    ui->error_plot->graph(0)->setData(t, last_solution_adi.errors);
    ui->error_plot->graph(1)->setData(t, last_solution_fs.errors);
    ui->error_plot->xAxis->setRange(0.0, 1.0);
    ui->error_plot->yAxis->setRange(0.0, last_solution_fs.max_error);
    ui->error_plot->replot();

    ui->solution_plot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    ui->solution_plot->axisRect()->setupFullAxesBox(true);
    ui->solution_plot->xAxis->setLabel("y");
    ui->solution_plot->yAxis->setLabel("x");


    QCPColorMap *colorMap = new QCPColorMap(ui->solution_plot->xAxis, ui->solution_plot->yAxis);
    colorMap->data()->setSize(ui->nx->value() + 1, ui->ny->value() + 1);
    colorMap->data()->setRange(QCPRange(0, eq.l1), QCPRange(0, eq.l2));



    for(int i = 0; i <= ui->nx->value(); ++i) {
       for(int j = 0; j <= ui->ny->value(); ++j) {
          // std::cout << result[i][j] << ' ';
           colorMap->data()->setCell(j, i, last_solution_adi.layer2[i][j]);
       }
    }

    QCPColorScale *colorScale = new QCPColorScale(ui->solution_plot);
    ui->solution_plot->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    colorScale->axis()->setLabel("SOLUTION");

    colorMap->setGradient(QCPColorGradient::gpPolar);
    colorMap->rescaleDataRange();
    QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->solution_plot);
    ui->solution_plot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    ui->solution_plot->rescaleAxes();
    ui->solution_plot->replot();
}

void MainWindow::on_prev_button_clicked()
{

}

void MainWindow::on_next_button_clicked()
{

}
