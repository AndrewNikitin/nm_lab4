#include "mainwindow.h"
#include "parabolic2d.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Parabolic2DEquation eq1(
            1.0, 0.0, 0.0, 0.0, [](double x, double y, double t){ return - x * y * sin(t);},
            [](double x, double y){ return x * y;},
            0.0, 1.0, [](double y, double t){ return 0 /* y*cos(t) */; },
            0.0, 1.0, [](double y, double t){ return 1.0 * y*cos(t); },
            0.0, 1.0, [](double x, double t){ return 0 /* x*cos(t) */;},
            0.0, 1.0, [](double x, double t){ return 1.0 * x*cos(t); },
            1.0, 1.0
    );
    Parabolic2DEquation eq2(
                1.0, 0.0, 0.0, 0.0, [](double x, double y, double t){return 0;},
                [](double x, double y){return cos(2*x) * cosh(y);},
                0.0, 1.0, [](double y, double t){return cosh(y) * exp(-3*t);},
                0.0, 1.0, [](double y, double t){return 0;},
                0.0, 1.0, [](double x, double t){return cos(2*x) * exp(-3*t);},
                0.0, 1.0, [](double x, double t){return (5.0/4.0) * cos(2*x) * exp(-3*t);},
                M_PI_4, log(2)
    );
    std::function<double(double, double, double)> solution1([](double x, double y, double t){return x * y * cos(t);});
    std::function<double(double, double, double)> solution2([](double x, double y, double t){return cosh(y) * cos(2*x) * exp(-3*t);});
    MainWindow w(eq2, solution2);
    w.show();
    return a.exec();
}
