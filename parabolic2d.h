#ifndef LAB4_PARABOLIC2D_H
#define LAB4_PARABOLIC2D_H


#include <iostream>
#include <functional>
#include <vector>
#include <cmath>
#include <QVector>

using VecDouble = QVector<double>;
using Layer = QVector<VecDouble>;

class Parabolic2DEquation
{
    /// u_t = a * (u_xx + u_yy) + b * u_x + c *u_y + du + f
    /// u(x, y, 0) = phi(x, y)
    /// alpha1 * u_x(0, y, t) + beta1 * u(0, y, t) = gamma1(y, t)
    /// alpha2 * u_x(l1,y, t) + beta2 * u(l2,y, t) = gamma2(y, t)
    /// alpha3 * u_y(x, 0, t) + beta3 * u(x, 0, t) = gamma3(x, t)
    /// alpha4 * u_y(x,l2, t) + beta4 * u(x,l2, t) = gamma4(x, t)
public:
    double a, b, c, d;
    std::function<double(double, double, double)> f;
    std::function<double(double, double)> phi;

    double alpha1, beta1;
    std::function<double(double, double)> gamma1;

    double alpha2, beta2;
    std::function<double(double, double)> gamma2;

    double alpha3, beta3;
    std::function<double(double, double)> gamma3;

    double alpha4, beta4;
    std::function<double(double, double)> gamma4;

    double l1;
    double l2;

    Parabolic2DEquation(
            double a, double b, double c, double d, std::function<double(double, double, double)> &&f,
            std::function<double(double, double)> &&phi,
            double alpha1, double beta1, std::function<double(double, double)> &&gamma1,
            double alpha2, double beta2, std::function<double(double, double)> &&gamma2,
            double alpha3, double beta3, std::function<double(double, double)> &&gamma3,
            double alpha4, double beta4, std::function<double(double, double)> &&gamma4,
            double l1, double l2
    )
    {
        this->a = a;
        this->b = b;
        this->c = c;
        this->d = d;
        this->f = f;
        this->phi = phi;
        this->alpha1 = alpha1;
        this->alpha2 = alpha2;
        this->alpha3 = alpha3;
        this->alpha4 = alpha4;
        this->beta1 = beta1;
        this->beta2 = beta2;
        this->beta3 = beta3;
        this->beta4 = beta4;
        this->gamma1 = gamma1;
        this->gamma2 = gamma2;
        this->gamma3 = gamma3;
        this->gamma4 = gamma4;
        this->l1 = l1;
        this->l2 = l2;
    }
};

struct SolutionRecord
{
    double hx, hy, tau;
    double l1, l2, t_max;
    int x_steps, y_steps, t_steps;

    double max_error;
    VecDouble errors;

    Layer layer1, layer2, layer3;
};

class Solver
{
private:
    static void TMAVertical(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, Layer &layer, int row, int size);
    static void TMAHorizontal(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, Layer &layer, int col, int size);
public:
    void ADIMethod(Parabolic2DEquation &eq, int x_steps, int y_steps, int t_steps, double t_max,
                   std::function<double(double, double, double)> &solution, SolutionRecord &solution_record);
    void FSMethod(Parabolic2DEquation &eq, int x_steps, int y_steps, int t_steps, double t_max,
                  std::function<double(double, double, double)> &solution, SolutionRecord &solution_record);
};

void TestMethods(int x_steps, int y_steps, int t_steps);

#endif //LAB4_PARABOLIC2D_H
